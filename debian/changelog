faac (1.30-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 01 Apr 2020 12:35:06 +0000

faac (1.30-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat

  [ Fabian Greffrath ]
  * Upstream moved to GitHub, adapt Homepage field
    and debian/watch file accordingly.
  * New upstream version 1.30
    + Check index ranges before dereferencing book arrays
      (CVE-2018-19886, CVE-2018-19887, CVE-2018-19889,
      CVE-2018-19890 CVE-2018-19891) Closes: #915763.
    + Add stdint.h header inclusions, Closes: #928038.
  * Remove all patches, applied upstream.
  * Bump debhelper-compat to 12.
  * Bump Standards-Version to 4.4.1.
  * Rules-Requires-Root: no.
  * Add Build-Depends-Package lines to the symbols file.
  * Add "usr/lib/*/*.la" to the debian/not-installed file.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 17 Oct 2019 20:58:54 +0200

faac (1.29.9.2-2) unstable; urgency=medium

  * Add some more post-release commits from the upstream GIT repo.
  * Add */*.la to debian/clean.
  * Upload to unstable.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 22 Sep 2018 21:14:07 +0200

faac (1.29.9.2-1) experimental; urgency=medium

  * Upload to experimental, let's first see if DRM still works.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Fabian Greffrath ]
  * New upstream version 1.29.9.2 (Closes: #900116).
  * Apply all post-1.29.9.2 commits from upstream's GIT repo.
  * Bump debhelper compat to 11.
  * Bump Standards-Version to 4.1.4.
  * Update debian/copyright for some removed or relicensed files.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 02 Jun 2018 20:35:38 +0200

faac (1.29.9-1) unstable; urgency=medium

  * New upstream version 1.29.9

 -- Fabian Greffrath <fabian@debian.org>  Mon, 06 Nov 2017 17:10:41 +0100

faac (1.29.7.7-1) unstable; urgency=medium

  * New upstream version 1.29.7.7.
  * Update debian/copyright for recently added or removed source files.
  * Bump Standards-Version to 4.1.0.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 30 Sep 2017 14:40:40 +0200

faac (1.29.7.4-1) unstable; urgency=medium

  * New upstream version 1.29.7.4.
  * Remove files from copyright file that were dropped upstream.
  * Fix spelling-error-in-changelog lintian warning.
  * Fix syntax errors in override rules.

 -- Fabian Greffrath <fabian@debian.org>  Wed, 06 Sep 2017 21:17:31 +0200

faac (1.29.7.2-1) unstable; urgency=medium

  * New upstream version 1.29.7.2.
  * Fix useless-autoreconf-build-depends lintian warning.
  * Explicitly build DRM variant, it isn't built by default anymore.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 26 Aug 2017 21:49:25 +0200

faac (1.29.3+git20170724-1) unstable; urgency=medium

  * New upstream version 1.29.3+git20170724 (commit 3ba209).
  * Adjust debian/copyright.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 24 Jul 2017 17:52:05 +0200

faac (1.29.2-1) unstable; urgency=medium

  * New upstream version 1.29.2.
    + New MP4 output module written from scratch (Closes: #797838).
  * Remove debian/README.Debian and untangle debian/rules,
    support for building against libmp4v2 has been removed upstream.
  * Update debian/copyright.
  * Append "--no-undefined" to LDFLAGS.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 17 Jul 2017 21:51:39 +0200

faac (1.29+git20170704-1) unstable; urgency=medium

  * New upstream version 1.29+git20170704 (commit 451843).
    + Fixes CVE-2017-9129 CVE-2017-9130 (Closes: 865909).
  * Update debian/watch file.
  * Remove debian/README.source and debian/gbp.conf files,
    they do not apply anymore.
  * Remove Maia Kozheva from Uploaders (Closes: #829316).
  * Bump Standards-Version to 4.0.0:
    + Refer to MPL-1.1 in common-licenses in debian/copyright.
  * Bump debhelper compat to 10.
  * Fix spelling-error-in-readme-debian lintian warning.
  * Fix vcs-field-uses-insecure-uri lintian warning.
  * Enable all hardening flags.

 -- Fabian Greffrath <fabian@debian.org>  Wed, 05 Jul 2017 20:13:41 +0200

faac (1.28+cvs20151130-1) unstable; urgency=medium

  * Imported Upstream version 1.28+cvs20151130
    + Fixes compilation without limbp4v2.
    + Fixes encoding from non-seekable raw input (LP: #1517608).
      Thanks to Sebastian Wilhelmi for the patch.
  * Remove patches, applied upstream.
  * Update my email address to my Debian account.
  * Explicitly disable building with MP4V2 in debian/rules unless
    DEB_BUILD_OPTIONS=mp4v2 is given.
  * Add README.Debian to explain why MP4 container support is disabled in
    the Debian package and how to rebuild it locally with MP4 support.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 30 Nov 2015 08:09:08 +0100

faac (1.28+cvs20150510-1) unstable; urgency=medium

  * New upstream CVS snapshot.
  * Add debian/README.source to document how the Debian source tarball was
    created and force xz compression in debian/gbp.conf.
  * Remove all patches that were either applied, solved differently or
    disapproved upstream:
    + autotools-compat.patch: Disapproved upstream.
    + build-fix.patch: Does not apply anymore.
    + symbol-visibility.patch: Does not apply anymore.
    + format-security.patch: Applied upstream.
    + exposed-API.patch: Applied upstream.
    + external-libmp4v2.patch: Does not apply anymore.
    + external-libmp4v2_r479.patch: Applied upstream.
    + libfaac_drm.patch: Does not apply anymore.
  * Apply patch from upstream bug tracker to fix compilation without libmp4v2.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Mon, 11 May 2015 14:46:31 +0200

faac (1.28-7) unstable; urgency=medium

  * Build the DRM version of the library as well as the normal version,
    thanks Julian Cable for the idea and the patch!
  * Remove Andres Mejia from Uploaders (Closes: #743523).
  * Remove debian/source/local-options, they are default now.
  * Fix most autotools warnings
  * Bump "Standards-Version" to 3.9.6.
  * Run "wrap-and-sort -asb".

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 30 Apr 2015 18:14:49 +0200

faac (1.28-6) unstable; urgency=low

  * Add "XS-Autobuild: yes" to debian/control and clarify in debian/copyright
    that this package can legitimately and technically be auto-built
    (Closes: #729145).

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 21 Nov 2013 10:03:07 +0100

faac (1.28-5) unstable; urgency=low

  [ Andres Mejia ]
  * Disable mp4v2 support.
    This only disables mp4v2 for the faac utility program. The faac
    utility is GPL-2 but the mp4v2 library is MPL-1.1. The two licenses
    are incompatible with each other.

  [ Reinhard Tartler ]
  * Revise debian/copyright
  * Bump standards version (no changes needed)

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 22 Jun 2013 15:09:31 +0200

faac (1.28-4) unstable; urgency=low

  * Upload to Debian. (Closes: #665318)
  * Change my contact info to use my @debian.org email.
  * Bump to Standards-Version 3.9.3.
  * Make dev package multiarch installable.
  * Add Vcs-* entries.

 -- Andres Mejia <amejia@debian.org>  Thu, 22 Mar 2012 21:45:48 -0400

faac (1.28-3) unstable; urgency=low

  * Use updated external libmp4v2 if available.
  * Port faac to the iTMF Generic and Tags API.
  * Add Build-Depends: libmp4v2-dev (>= 1.9.1).

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Mon, 20 Feb 2012 15:06:05 +0100

faac (1.28-2) unstable; urgency=low

  * Build faac against the public API exposed in <faac.h>
    instead of the private API defined in "libfaac/frame.h".
  * Simplify symbol-visibility.patch accordingly.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Tue, 07 Feb 2012 14:28:52 +0100

faac (1.28-1) unstable; urgency=low

  * Update my e-mail address in Uploaders field.
  * Update debian/copyright.
  * Ship upstream manpage instead if our own one.
  * Fix dh invocation in debian/rules.
  * Set appropriate symbol visibility attributes.
  * Add debian/libfaac0.symbols file.
  * Fix format string security error.
  * Multi-Archify.
  * TODO: Port to libmp4v2-r479 API.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Mon, 06 Feb 2012 14:57:15 +0100

faac (1.28-0fab3) unstable; urgency=low

  [ Andres Mejia ]
  * Various fixes for getting faac working on current Debian sid.
  * Include myself in Uploaders field.
  * Clarify comment about build fixes in patch.
  * Change this package section to non-free due to its conflicting
    licensing terms.
  * Don't include libtool file.

  [ Maia Kozheva ]
  * Add .gitignore.
  * Remove explicit quilt dependency, migrate to 3.0 source format.
  * Remove autoreconf.patch, using dh_autoreconf.
  * debian/control:
    - Reword binary package descriptions to remove initial articles.
    - Bump Standards-Version to 3.9.2.
    - Update maintainer field.
    - Add myself to uploaders.
  * debian/control, debian/rules:
    - Remove quilt, add dh-autoreconf support.
  * debian/faac.manpages, debian/manpages/faac.1:
    - Add manpage from Ubuntu version 1.26-0.1ubuntu2, with manpage hyphen
      issues corrected.

 -- Maia Kozheva <sikon@ubuntu.com>  Thu, 09 Jun 2011 11:24:43 +0700

faac (1.28-0fab2) unstable; urgency=low

  * debian/control, debian/rules: Ported from debhelper (>= 7) to cdbs.
  * debian/control: Wrapped Build-Depends and Depends fields.

 -- Fabian Greffrath <fabian@greffrath.com>  Mon, 29 Jun 2009 12:00:00 +0200

faac (1.28-0fab1) unstable; urgency=low

  * Initial release.

 -- Fabian Greffrath <fabian@greffrath.com>  Mon, 01 Jun 2009 00:00:00 +0200
